//
//  SignUpViewController.swift
//  Demo
//
//  Created by Santiago Apaza on 4/21/18.
//  Copyright © 2018 PUCP. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {
    
    fileprivate var isCheckedToS = false
    
    @IBOutlet weak var checkToST: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func checingkToS(_ sender: Any) {
        isCheckedToS = !isCheckedToS
        
        if isCheckedToS {
            checkToST.setBackgroundImage(UIImage(named: "ic_check_box"), for: .normal)
        } else {
            checkToST.setBackgroundImage(UIImage(named: "ic_check_box_outline_blank"), for: .normal)
        }
    }
    
    @IBAction func register(_ sender: Any) {
        let alert = UIAlertController(title: "Éxito!", message: "Sus datos han sido registrados.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Gracias", style: .default, handler: {
            action in
            self.dismiss(animated: true, completion: nil)
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
}
